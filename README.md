An interface for a SQLite database that is compatible with the MGI Database interface.

# SQLite dll Version: `3.28.0`

>SQLite is a C-language library that implements a small, fast, self-contained, high-reliability, full-featured, SQL database engine. SQLite is the most used database engine in the world. -<cite>https://sqlite.org/index.html</cite>

This package contains all of the code needed to interface with a SQLite database.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_

```

```
