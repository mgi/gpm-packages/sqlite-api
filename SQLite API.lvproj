﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="GPM Packages" Type="Folder">
			<Property Name="GPM" Type="Bool">true</Property>
			<Item Name="@mgi" Type="Folder">
				<Item Name="database-interface" Type="Folder">
					<Item Name="Database" Type="Folder">
						<Item Name="Connection" Type="Folder">
							<Item Name="Escape Character.vi" Type="VI" URL="../gpm_packages/@mgi/database-interface/Database/Connection/Escape Character.vi"/>
						</Item>
						<Item Name="Database.lvlib" Type="Library" URL="../gpm_packages/@mgi/database-interface/Database/Database.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/database-interface/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/database-interface/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/database-interface/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/database-interface/README.md"/>
				</Item>
				<Item Name="query-builder" Type="Folder">
					<Item Name="Query Builder" Type="Folder">
						<Item Name="Query Builder.lvclass" Type="LVClass" URL="../gpm_packages/@mgi/query-builder/Query Builder/Query Builder.lvclass"/>
					</Item>
					<Item Name=".gitignore" Type="Document" URL="../gpm_packages/@mgi/query-builder/.gitignore"/>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/query-builder/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/query-builder/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/query-builder/LICENSE"/>
					<Item Name="Query Builder.lvproj" Type="Document" URL="../gpm_packages/@mgi/query-builder/Query Builder.lvproj"/>
					<Item Name="readme.md" Type="Document" URL="../gpm_packages/@mgi/query-builder/readme.md"/>
				</Item>
			</Item>
		</Item>
		<Item Name="Source" Type="Folder">
			<Item Name="MGI-SQLite.lvlib" Type="Library" URL="../Source/MGI-SQLite.lvlib"/>
			<Item Name="sqlite3_x64.dll" Type="Document" URL="../Source/sqlite3_x64.dll"/>
			<Item Name="sqlite3_x86.dll" Type="Document" URL="../Source/sqlite3_x86.dll"/>
		</Item>
		<Item Name="Test" Type="Folder">
			<Item Name="Test.vi" Type="VI" URL="../Test/Test.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
