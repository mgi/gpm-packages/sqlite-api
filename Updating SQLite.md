1. Download the following `Precompiled Binaries for Windows` binaries:
   1. sqlite-dll-win32-x86-<version>.zip
   2. sqlite-dll-win32-x64-<version>.zip
2. Extract the dlls and rename `sqlite.dll` to include the appropriate architecture post-fix. Put them in the `Source` folder
3. Validate that version updates did not introduce any bugs
